export default function ({ store, redirect }) {
  // If the user is not authenticated
  if (store.state.User == false || store.state.basket.length==0) {
    return redirect('/')
  }
}
