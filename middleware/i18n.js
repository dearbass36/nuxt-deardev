import axios from 'axios'
import {
  loadMessages
} from '~/plugins/i18n'

export default async ({store}) => {
  try {
    let locale = await this.$cookies.get('locale');
    if (locale != undefined) {
      await store.commit('lang/SET_LOCALE', {
        locale
      })
      axios.defaults.headers.common['Accept-Language'] = await locale
    }

  } catch (err) {}

  await loadMessages(store.getters['lang/locale'])
}
