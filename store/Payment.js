const state = () => ({
  address_id: null,
  delivery: null,
  payment: {
    type: '',
    no: null,
    name: null,
    ddyy: null,
    pwd: null
  }
})

const getters = {

}

const mutations = {
  SET_DELIVERY(state, data) {
    state.address_id = data.address_receiver;
    state.delivery = data.delivery;
  },
  SET_PAY(state, data) {
    state.payment.type = data.type;
    state.payment.no = data.no;
    state.payment.name = data.name;
    state.payment.ddyy = data.ddyy;
    state.payment.pwd = data.pwd;
  },
  CLEAR_PAYMENT(state) {
    state.address_id = null
    state.delivery = null
    state.payment.type = ''
    state.payment.no = null
    state.payment.name = null
    state.payment.ddyy = null
    state.payment.pwd = null
  }
}

const actions = {

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
