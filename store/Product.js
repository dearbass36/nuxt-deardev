const state = () => ({
  category: [],
  product: [],
  brand: []
})

const getters = {
  BrandByID: (state) => (id) => {
    return state.brand.find((q) => q.id == id)
  },
  ProductByBrand : (state) => (id) => {
    return state.product.filter((q) => q.b_id == id);
  },
  ParentCategory(state) {
    return state.category.filter((q) => q.c_subid == 0);
  },
  CategoryByID: (state) => (id) => {
    return state.category.find((q) => q.id == id)
  },
  subCategoryByID: (state) => (id) => {
    return state.category.filter((q) => q.c_subid == id)
  },
  ProductByCID: (state) => (id) => {
    let findsub = (category, matchID) => {
      let find = category.filter((q) => q.c_subid == matchID);
      Allarray.push(parseInt(matchID));
      if (find.length > 0) {
        for (let i in find) {
          findsub(category, find[i].id);
        }
      }
    }
    let Allarray = [];
    const Category = state.category;
    const Product = state.product;
    let CallFunction = findsub(Category, id);
    let product = [];
    for (let index in Allarray) {
      let result = Product.filter((q) => q.c_id == Allarray[index]);
      for (let x in result) {
        product.push(result[x]);
      }
    }
    return product;
  },
  newProduct(state) {
    return state.product.filter((q) => q.p_status == 2)
  },
  hotProduct(state) {
    return state.product.filter((q) => q.p_status == 3)
  },
  ProductByID: (state) => (id) => {
    return state.product.find((q)=>q.id == id);
  },
  RelateProduct: (state) => (id) => {
    let product = state.product.find((q)=>q.id == id);
    let category = product.c_id;
    let brand = product.b_id;
    return state.product.filter((q) => q.b_id == brand && q.id != id || q.c_id == category && q.id != id )
  }
}

const mutations = {
  setCategory(state, data) {
    state.category = data;
  },
  setProduct(state, data) {
    state.product = data;
  },
  setBrand(state, data) {
    state.brand = data;
  }
}

const actions = {
  async nuxtServerInit({
    commit
  }) {
    console.log(commit);
  },
  async getCategory({
    commit
  }) {
    await this.$axios.$get('category')
      .then((res) => {
        commit('setCategory', res)
      })
      .catch((err) => {
        console.log(err)
      })
  },
  async getProduct({
    commit
  }) {
    await this.$axios.$get('product')
      .then((res) => {
        commit('setProduct', res)
      })
      .catch((err) => {
        console.log(err)
      })
  },
  async getBrand({
    commit
  }) {
    await this.$axios.$get('brand')
      .then((res) => {
        commit('setBrand', res)
      })
      .catch((err) => {
        console.log(err)
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
