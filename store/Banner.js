const state = () => ({
  bannerSlide: [],
  bannerTopPage: [],
  bannerMidPage: [],
  bannerRecommended: []
})

const getters = {

}

const mutations = {
  setBanner(state, data) {
    state.bannerSlide = data.filter((q) => q.bh_type == 1);
    state.bannerTopPage = data.filter((q) => q.bh_type == 4);
    state.bannerMidPage = data.filter((q) => q.bh_type == 2);
    state.bannerRecommended = data.filter((q) => q.bh_type == 3);
  }
}

const actions = {

  async getBanner({
    commit
  }) {
    await this.$axios.$get('banner')
      .then((res) => {
        commit('setBanner', res)
      })
      .catch((err) => {
        console.log(err)
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
