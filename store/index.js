import axios from 'axios'
export const state = () => ({
  User: false,
  token: null,
  follow: [],
  basket: [],
  path: process.env.pathServer,
  address: null
})
export const getters = {
  user: state => state.User,
  token: state => state.token,
  path: state => state.path,
  follow: state => state.follow,
  basket: state => state.basket,
  address: state => state.address,
  addressByID: (state) => (id) => {
    return state.address.find((q)=>q.id == id);
  }
}
export const mutations = {
  SET_LANG(state, locale) {
    if (state.locales.indexOf(locale) !== -1) {
      state.locale = locale
    }
  },
  SET_USER(state, data) {
    state.User = data;
    if (data === false) {
      state.follow = [];
      state.basket = [];
    } else {
      state.follow = data.follow;
      state.basket = data.basket;
    }

  },
  SET_TOKEN(state, token) {
    state.token = token
  },
  CHECK_FOLLOW(state, data) {
    let Obj = state.follow;
    let index = _.findKey(Obj, ['p_id', data.id]);
    if (index == undefined) {
      state.follow.push({
        id: null,
        product: data,
        p_id: data.id,
        u_id: state.User.id
      })
    } else {
      state.follow.splice(index, 1)
    }
  },
  SET_FOLLOW(state, data) {
    state.follow = data;
  },
  CHECK_BASKET(state, data) {
    let Obj = state.basket;
    let index = _.findKey(Obj, ['p_id', data.id]);
    if (index == undefined) {
      state.basket.push({
        id: null,
        product: data,
        p_id: data.id,
        u_id: state.User.id,
        qty: 1
      })
    } else {
      state.basket[index].qty++;
    }
  },
  SET_BASKET(state, data) {
    state.basket = data
  },
  REMOVE_BASKET(state, id) {
    let Obj = state.basket;
    let index = _.findKey(Obj, ['id', id]);
    state.basket.splice(index, 1);
  },
  async IncrementBasket(state, id) {
    let result = await state.basket.find((q) => q.id === id);
    let qty = await result.qty++;
    await this.$axios.$put('updateBasketQty',{
      id : id,
      status : 1
    }).then((res)=>{

    })
  },
  async DecrementBasket(state, id) {
    let result = await state.basket.find((q) => q.id === id);
    if (result.qty != 1) {
      let qty = await result.qty--;
      await this.$axios.$put('updateBasketQty',{
        id : id,
        status : 2
      }).then((res)=>{

      })
    }
  },
  SET_ADDRESS(state,data) {
    state.address = data
  }
}
export const actions = {
  async nuxtServerInit({ dispatch }, { app }) {
    const locale = await app.$cookies.get('locale');
    if(locale) await dispatch('lang/setLocale', {locale})
  },
  async GET_USER({commit}) {
    await this.$axios.$get('auth/user')
      .then((res) => {
        const user = res.data;
        commit('SET_USER', user);
      })
      .catch((err) => {
        commit('SET_USER', false)
        commit('SET_TOKEN', false)
        this.$cookies.remove('token')
      })
  },
  async PUT_FOLLOW({commit,state}, data) {
    await commit('CHECK_FOLLOW', data)
    await this.$axios.$put('follow', {
        u_id: state.User.id,
        p_id: data.id
      })
      .then((res) => {
        commit('SET_FOLLOW', res)
      })
      .catch((err) => {})
  },
  async PUT_BASKET({commit,state}, data) {
    await commit('CHECK_BASKET', data)
    await this.$axios.$put('basket', {
        u_id: state.User.id,
        p_id: data.id
      })
      .then((res) => {
        commit('SET_BASKET', res)
      })
      .catch((err) => {})
  },
  async getAddress({commit,state}) {
    console.log(state.User)
    await this.$axios.$get('addresses/'+state.User.id)
    .then((res) => {
      commit('SET_ADDRESS', res)
    })
  }
}
export const strict = false
