export default function ({ app , store, $axios, redirect }) {
  const token = app.$cookies.get('token')
  if(token != null) {
    $axios.setToken(token, 'Bearer')
    store.dispatch('GET_USER');
    store.commit('SET_TOKEN',token);
  }
  $axios.onRequest(config => {
    console.log('Making request to ' + config.url)
  })
  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    if (code === 400) {
      redirect('/400')
    }
  })
}
