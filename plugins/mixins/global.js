import Vue from 'vue'
import _ from 'lodash'
const globalFunction = {
  install(Vue, Options) {
    Vue.mixin({
      methods: {
        cutTag(text) {
          let result = text.replace(/<(?:.|\n)*?>/gm, '');
          let result2 = result.replace(/&(?:.|\n)*?;/gm, '');
          let split = result2.split(" ").filter((q) => q != '');
          let string = split.join("");
          return string.slice(0, 100) + "...";
        },
        formatDate(date) {
          const realDate = date.split(' ');
          var month = new Array();
          month[0] = "January";
          month[1] = "February";
          month[2] = "March";
          month[3] = "April";
          month[4] = "May";
          month[5] = "June";
          month[6] = "July";
          month[7] = "August";
          month[8] = "September";
          month[9] = "October";
          month[10] = "November";
          month[11] = "December";
          let d = new Date(_.take(realDate));
          let day = d.getDate();
          let m = d.getMonth();
          let year = d.getFullYear();
          return month[m] + " " + day + " " + year;
        },
        time() {
          const vm = this;
          let data = vm.Flashsale;
          if (data != undefined) {
            let a = vm.$moment(data.fs_dateend);
            let b = vm.$moment(vm.datenow);
            let diff = a.diff(b)

            if (diff < 0) {
              vm.$axios.$put('flashsale/' + data.id, {
                  params: {
                    status: 0
                  }
                })
                .then((res) => {})
                .catch((err) => {})
                .then(() => {
                  if (vm.ProductByID != undefined) {
                    vm.Flashsale = null;
                  } else {
                    vm.$store.dispatch('Promotion/getFlashsale');
                    vm.$store.dispatch('Product/getProduct');
                    vm.$store.dispatch('GET_USER');
                  }
                })
            } else {
              let duration = vm.$moment.duration(diff);
              let hours = parseInt(duration.asHours());
              let minutes = parseInt(duration.asMinutes()) - (hours * 60);
              let seconds = (parseInt(duration.asSeconds()) - (minutes * 60) - (hours * 3600));
              if (isNaN(hours) || isNaN(minutes) || isNaN(seconds)) {

              } else {
                vm.dateFs.hh = vm.twoDigit(hours);
                vm.dateFs.mm = vm.twoDigit(minutes);
                vm.dateFs.ss = vm.twoDigit(seconds);
              }
            }
          } else {
            vm.dateFs.ss = "00";
            vm.dateFs.mm = "00";
            vm.dateFs.hh = "00";
          }
          vm.datenow = vm.$moment().format("YYYY-MM-DD HH:mm:ss");
          setTimeout(vm.time, 1000)
        },
        twoDigit(txt) {
          let text = `${txt}`;
          if (text.length < 2) {
            return "0" + text;
          } else {
            return text;
          }
        },
        CalDiscount(d_price, price) {
          let discount = 100 - ((d_price * 100) / price);
          return parseInt(discount);
        },
        loveAdd(item) {
          if (this.user == false) return this.$router.push('/auth/Login');
          this.$store.dispatch("PUT_FOLLOW", item);
        },
        checkLove(id) {
          if (this.user == false) return false;
          let result = this.follow.find((q) => q.p_id == id);
          if (result != undefined) {
            return true;
          } else {
            return false;
          }
        },
        basketAdd(item) {
          if (this.user == false) return this.$router.push('/auth/Login');
          this.$store.dispatch("PUT_BASKET", item);
        },
        async basketRemove(id) {
          await this.$store.commit('REMOVE_BASKET', id);
          await this.$axios.$delete('remove_basket/' + id)
            .then((res) => {
              this.$store.commit('SET_BASKET', res)
            })
            .catch((err) => {})
          if (this.basket.length == 0 && this.$route.path == '/Pay') return this.$router.push("/")
        },
        PriceType(item) {
          try {
            let discount = item.flashsale.find((q) => q.flash_sale != null).fsd_price
            let percen = this.CalDiscount(discount, item.p_price)
            return `<div class="product_price">
                            <div class="d-flex flex-column justify-content-center">
                              <p class="price">${ this.formatMoney(discount) } บาท</p>
                              <p class="cut">
                                <span>${ this.formatMoney(item.p_price) } บาท</span>
                              </p>
                            </div>
                            <p class="sell">
                              -${percen}%
                            </p>
                          </div>`
          } catch (err) {
            if (item.p_dis_price != null || item.p_dis_price != undefined) {
              let percen = this.CalDiscount(item.p_dis_price, item.p_price)
              return `<div class="product_price">
                              <div class="d-flex flex-column justify-content-center">
                                <p class="price">${ this.formatMoney(item.p_dis_price) } บาท</p>
                                <p class="cut">
                                  <span>${ this.formatMoney(item.p_price) } บาท</span>
                                </p>
                              </div>
                              <p class="sell">
                                -${percen}%
                              </p>
                            </div>`
            } else {
              return `<div class="product_price">
                        <div class="d-flex flex-column justify-content-center">
                          <p class="price">${ this.formatMoney(item.p_price) } บาท</p>
                        </div>
                      </div>`
            }
          }
        },
        formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
          try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
            const negativeSign = amount < 0 ? "-" : "";
            let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
            let j = (i.length > 3) ? i.length % 3 : 0;
            return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g,
              "$1" + thousands);
          } catch (e) {}
        },
      }
    })
  }
}

Vue.use(globalFunction);
